package clipto.flow.domain

enum class Status {

    STARTED,
    COMPLETED,
    PENDING,
    CANCELED,
    REJECTED,
    BLOCKED,
    DECLINED,
    TERMINATED,
    EXPIRED,
    ERROR

}