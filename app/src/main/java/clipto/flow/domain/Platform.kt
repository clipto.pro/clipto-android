package clipto.flow.domain

data class Platform(
    val id: String,
    val name: String
)