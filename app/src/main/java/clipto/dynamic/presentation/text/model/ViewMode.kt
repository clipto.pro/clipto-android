package clipto.dynamic.presentation.text.model

enum class ViewMode {
    TEXT,
    FORM,
    PREVIEW
}