package clipto.dynamic.presentation.field.model

enum class ViewMode {
    INSERT,
    COPY,
    VIEW,
    EDIT,
    FILL
}
