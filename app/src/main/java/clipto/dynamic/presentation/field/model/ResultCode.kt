package clipto.dynamic.presentation.field.model

enum class ResultCode {

    DELETE,

    INSERT,

    COPY,

    UPDATE

}