package clipto.presentation.common.dialog.select.options

import com.wb.clipboard.R

enum class OrderingType(val titleRes: Int) {

    LABEL_ASC(R.string.select_option_ordering_label_asc),
    LABEL_DESC(R.string.select_option_ordering_label_desc),
    VALUE_ASC(R.string.select_option_ordering_value_asc),
    VALUE_DESC(R.string.select_option_ordering_value_desc)

}
