package clipto.presentation.runes.keyboard_companion

enum class CompanionMode {

    AUTO_DETECTED,
    USER_SEARCH,
    DETACHED,
    HIDDEN

}