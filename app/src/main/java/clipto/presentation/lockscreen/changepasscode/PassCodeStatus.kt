package clipto.presentation.lockscreen.changepasscode

enum class PassCodeStatus {
    ENTER_EXISTING_PASS,
    SET_NEW_PASS,
    CONFIRM_NEW_PASS
}