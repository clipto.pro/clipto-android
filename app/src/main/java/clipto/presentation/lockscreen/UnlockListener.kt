package clipto.presentation.lockscreen

interface UnlockListener {
    fun onUnlocked()
    fun onUnauthorized()
}