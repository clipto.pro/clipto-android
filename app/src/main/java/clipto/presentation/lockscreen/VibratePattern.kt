package clipto.presentation.lockscreen

enum class VibratePattern {
    PIN_OK, PIN_WRONG
}