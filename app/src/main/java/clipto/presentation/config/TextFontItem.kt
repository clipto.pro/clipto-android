package clipto.presentation.config

import clipto.domain.Font

data class TextFontItem(
    val font: Font,
    val selected: Boolean)