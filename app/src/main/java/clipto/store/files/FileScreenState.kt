package clipto.store.files

import clipto.domain.AttributedObjectScreenState
import clipto.domain.FileRef

typealias FileScreenState = AttributedObjectScreenState<FileRef>