package clipto.store.clipboard.data

object ClipboardActions {

    const val ACTION_SEARCH = "track_clipboard_search"

    const val SHARE_ACTION_EDIT_NOTE = "share_action_edit_note"
    const val SHARE_ACTION_NEW_NOTE = "share_action_new_note"

    const val ACTION_AUTHORIZE = "act_authorize"

}