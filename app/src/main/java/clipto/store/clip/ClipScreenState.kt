package clipto.store.clip

import clipto.domain.AttributedObjectScreenState
import clipto.domain.Clip

typealias ClipScreenState = AttributedObjectScreenState<Clip>