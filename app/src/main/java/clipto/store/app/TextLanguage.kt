package clipto.store.app

data class TextLanguage(
    val code: String,
    val confidence: Float
)