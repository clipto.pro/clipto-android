package clipto.exception

class ValidationException(val errorMessage: String) : RuntimeException()