package clipto.domain

enum class FocusMode {
    TEXT,
    TEXT_AUTO_SCROLL,
    TITLE,
    DESCRIPTION,
    ABBREVIATION,
    TAGS,
    PREVIEW,
    NONE
}