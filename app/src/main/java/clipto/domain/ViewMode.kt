package clipto.domain

enum class ViewMode {
    VIEW,
    EDIT,
    PREVIEW,
    READONLY,
}