package clipto.common.presentation.text

import android.view.View

interface MyClickableSpan {

    fun onClick(widget: View)

}