package clipto.common.presentation.mvvm

interface LoadingStateProvider {

    fun setLoadingState()

    fun setLoadedState()

}