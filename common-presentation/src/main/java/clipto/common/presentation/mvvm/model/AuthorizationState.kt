package clipto.common.presentation.mvvm.model

/**
 * Created by aash on 19/03/2018.
 */
enum class AuthorizationState {

    AUTHORIZED,
    NOT_AUTHORIZED,
    AUTHORIZATION_REQUIRED

}