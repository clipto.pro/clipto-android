package clipto.common.presentation.mvvm

interface ActivityBackPressConsumer {

    fun onBackPressConsumed(): Boolean

}